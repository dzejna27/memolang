import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main
{
	public static void main (String [] args) throws IOException
	{
		FileReader ff=new FileReader("src/words.txt");
		Scanner vstup=new Scanner (ff);
		PrintWriter fv=new PrintWriter("src/words_edit.txt");
		while (vstup.hasNextLine())
		{
			String line=vstup.nextLine();
            String tmp="";
			for(int i=0;i<line.length();i++)
			{
				 if (line.charAt(i)==',')
				     tmp+="\",\"";
				 else
				     tmp+=line.charAt(i);
			}
			System.out.println(tmp);
			fv.println(tmp);
		}
		ff.close();
        fv.close();
	}
}
