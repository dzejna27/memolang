package pexeso;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import pexeso.Picture.OnClick;

public class Game extends JPanel
{

	JGameStatus status;
	int lang, defLang, theme;
	BufferedImage image;
	int width, height;
	ArrayList<Picture> searching;
	ArrayList<Picture> showed;
	int [] points;
	int [] badPoints;
	int players;
	int actualPlayer = 0;
	Picture[] picArray;
	
	public Game(int lang, int count, int diff, int theme, JGameStatus status, int defLang)
	{
		this.status = status;
		this.defLang = defLang;
		this.lang=lang;
		this.theme=theme;
		players = count + 1;
		searching = new ArrayList<Picture>();
		showed = new ArrayList<Picture>();
		points = new int [players];
		badPoints = new int[players];
		ArrayList<Integer>list=new ArrayList<Integer>();
		int i;
		
		switch(diff)
		{
			case 0:
				width = 4;
				height = 3;
				break;
			case 1:
				width = 5;
				height = 4;
				break;
			case 2:
				width = 6;
				height = 5;
				break;
		}
		
		picArray = new Picture[width * height];
		int tmp = 0;
		setLayout(new GridLayout(width, height, 5, 5));

		try
		{
			image = ImageIO.read(new File("src/thumb.jpg"));
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
        
		switch (theme)
		{
		    case 0:
		    	for (i=0;i<50;i++)
		            list.add(i);
		    	break;
		    case 1:
		    	for (i=0;i<39;i++)
		            list.add(i);
		    	break;
		    case 2:	
		    	for (i=0;i<50;i++)
		            list.add(i);
		    	break;
		}
		/*for (i=0;i<50;i++)
            list.add(i);*/
		
		for (i = 0; i < picArray.length / 2; i++)
		{
			tmp = (int) (Math.round(Math.random() * (list.size()-1)));
			
			Picture pic1 = new Picture(list.get(tmp), image, lang, count, diff, theme, 0, defLang);
			pic1.setOnClickListener(new OnClick()
			{
				public void OnClicked(Picture pict)
				{
					click(pict);
				}
			});
			
			Picture pic2 = new Picture(list.get(tmp), image, lang, count, diff, theme, 1, defLang);
			pic2.setOnClickListener(new OnClick()
			{
				public void OnClicked(Picture pict)
				{
					click(pict);
				}
			});
			
			addRandom(picArray, pic1);
			addRandom(picArray, pic2);
			
			list.remove(tmp);
		}

		for (i = 0; i < picArray.length; i++)
			add(picArray[i]);
	}
	
	private void click(Picture card)
	{
		String [] window={"Na rade je hr�� / Now is playing no.: ", "Na rade je hr�� / Jetzt spielt: ", "Na rade je hr�� / Na �ade je hr��: ",
				"Now is playing no. / Jetzt spielt: ", "Na �ade je hr�� / Now is playing no.: ", "Na �ade je hr�� / Jetzt spielt: "};
	    String [] header={"�al�� hr�� / Next player", "�al�� hr�� / Der nachste Spieler", "�al�� hr�� / Dal�� hr��", 
	    		"Next player / Der nachste Spieler", "Dal�� hr�� / Next player", "Dal�� hr�� / Der nachste Spieler"};
	    String [] player={"hr�� / player", "hr�� / Spieler" , "hr��", "player / Spieler", "hr�� / player", "hr�� / Spieler"};
		String [] point={"bod / point", "bod / Punkt", "bod", "point / Punkt", "bod / point, bod / Punkt"};
		String [] points24={"body / points", "body / Punkte", "body", "points / Punkte", "body / points, body / Punkte"};	
		String [] points5={"bodov / points", "bodov / Punkte", "bodov / bod�", "points / Punkte", "bod� / points, bod� / Punkte"};		
		String [] badPoints24={"zl� body / bad points", "zl� body / schlechte Punkte", "zl� body", 
				"bad points / schlechte Punkte", "zl� body / bad points, zl� body /schlechte Punkte"};	
		String [] finMark={"V�sledn� zn�mka / Final mark", "V�sledn� zn�mka / Resultierend Marke", " Final mark / Resultierend Marke", 
				"V�sledn� zn�mka / Final mark", "V�sledn� zn�mka / Resultierend Marke"};
		
	    String pts="";
	    int mark = 0;
		for(int i = 0; i < showed.size(); i++)
			if(card.equals(showed.get(i)))
				return;
		
		if(searching.isEmpty()){
			if (card.lang2 == defLang)
				searching.add(card);
			else
				card.hidePexeso();
		}
		else
		{
			if(searching.size() == 1)
			{
				if((searching.get(0).part == card.part) && (searching.get(0).lang2 == card.lang2))
					return;
				
				if((searching.get(0).part == card.part) && (searching.get(0).lang2 != card.lang2))
				{
					showed.add(searching.get(0));
					showed.add(card);
					searching.clear();
					points[actualPlayer]++;
					status.finded = points[actualPlayer];
					status.repaint();
					
					
					if (showed.size()==picArray.length)
					{
						JFrame frame = new JFrame();
						frame.setSize(300, 300);
						frame.setTitle("Scores");
						frame.setBackground(Color.WHITE);
						frame.setVisible(true);
						frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						Box box = Box.createVerticalBox();
						if(players > 1){
							
							box.add(Box.createHorizontalStrut(10));
							for(int i = 0; i < players; i++)
							{
								if (points[actualPlayer]==1)
									pts = point[lang];
								else
								{	
									if (points[actualPlayer]>4)
										pts = points5[lang];
									else
										pts = points24[lang];
								}
								
								box.add(new JLabel(player[lang]+" "+(i + 1) + ": " + points[i] + " " + pts));
								box.add(Box.createVerticalStrut(10));
								box.add(Box.createHorizontalStrut(20));
							}
						}else
						{
							int tmp = ((points[0]-badPoints[0])/points[0])*100;
							if (tmp>=80)
								mark=1;
							else
								if  ((tmp<80) && (tmp>=60))
								     mark=2;
								else 
									if ((tmp<60) && (tmp>=40))
									 	mark=3;
									else
										if ((tmp<40) && (tmp>=20))
										mark=4;
									else
										mark=5;
							
							box.add(new JLabel(points24[lang]+": " + points[0]));
							box.add(new JLabel(badPoints24[lang] + ":" + badPoints[0]));
							box.add(Box.createVerticalStrut(10));
							box.add(new JLabel(finMark[lang] + ": " + mark));
						}
						frame.add(box);
						frame.setVisible(true);
					}
					return;
				}
				
				searching.add(card);
			}
			else
			{
				for(int i = 0; i < searching.size(); i++)
					searching.get(i).hidePexeso();
				
				badPoints[actualPlayer]++;
				searching.clear();
				card.hidePexeso();
				
				if(players != 1)
				{
					actualPlayer++;
					
					if(actualPlayer == players)
						actualPlayer = 0;
					
					JOptionPane.showMessageDialog(new JFrame(), window[lang] + (actualPlayer + 1), header [lang], JOptionPane.WARNING_MESSAGE);
					status.player = actualPlayer + 1;
					status.finded = points[actualPlayer];
					status.repaint();
				}
			}
		}
	
	}

	private void addRandom(Picture[] pics, Picture pic)
	{
		int pos;

		do
		{
			pos = (int) Math.round(Math.random() * (pics.length - 1));
		} 
		while (pics[pos] != null);

		pics[pos] = pic;
	}
}
