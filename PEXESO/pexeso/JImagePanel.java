package pexeso;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class JImagePanel extends JPanel
{
	private static BufferedImage ground;

	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.drawImage(ground, 0, 0, getWidth(), getHeight(), null);
	}

	public JImagePanel()
	{
		try
		{
			ground = ImageIO.read(new File("src/background.jpg"));
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
