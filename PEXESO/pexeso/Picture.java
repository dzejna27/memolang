package pexeso;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class Picture extends JPanel implements MouseListener
{
	private OnClick listener=null;
	private BufferedImage image;
	private BufferedImage pexeso;
	public int part;
	public int lang, count, diff, theme, lang2, defLang;
	private boolean clicked = false;

	public Picture(int part, BufferedImage img, int lang, int count, int diff, int theme,
			int lang2, int defLang)
	{
		image = img;
		this.part = part;
		this.lang = lang;
		this.defLang = defLang;
		this.count = count;
		this.diff = diff;
		this.theme = theme;
		this.lang2 = lang2;
		
		try
		{
			switch (theme)
			{
				case 0:
					pexeso = ImageIO.read(new File("src/"+part+".png"));
					break;
				case 1:
					pexeso = ImageIO.read(new File("src1/"+part+".png"));
					break;
				case 2:	
					pexeso = ImageIO.read(new File("src2/"+part+".png"));
					break;
			}
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		addMouseListener(this);
	}

	@Override
	protected void paintComponent(Graphics g)
	{

		String[] sk0 = { "krava", "krab", "krokod�l", "slon", "�irafa", "lev",
				"opica", "my�", "chobotnica", "panda", "tu�niak", "prasa",
				"ovca", "had", "korytna�ka", "medve�", "ma�ka", "som�r", "�ralok",
				"ka�ica", "ryba", "hus", "potkan", "mamut", "zebra", "mro�", "tiger",
				"sliepka", "b�k", "koza", "je�ko", "k��", "zajac", "nosoro�ec", "slim�k", 
				"pav�k", "v�ela", "veverica", "sova", "mor�a", "�kre�ok", "pes", "mot��", 
				"delf�n", "hroch", "ve�ryba", "vt�k", "�aba", "zlat� rybka", "papag�j"};
		String[] cz0 = { "kr�va", "krab", "krokod�l", "slon", "�irafa", "lev",
				"opice", "my�", "chobotnice", "panda", "tu���k", "prase",
				"ovce", "had", "�elva", "medv�d", "ko�ka", "osel", "�ralok",
				"kachna", "ryba"," husa", "potkan", " mamut", "zebra", "mro�", "tygr",
				"slepice", "b�k", "koza", "je�ek", "k��", "zaj�c", "nosoro�ec", "slim�k",
				"pavouk", "v�ela", "veverka", "sova", "mor�e", "k�e�ek", "pes", "mot�l", 
				"delf�n", "hroch", "velryba", "pt�k", "��ba", "zlat� rybka", "papou�ek"};
		String[] en0 = { "cow", "crab", "crocodile", "elephant", "giraffe",
				"lion", "monkey", "mouse", "octopus", "panda", "penguin",
				"pig", "sheep", "snake", "turtle", "bear", "cat", "donkey", "shark", 
				"duck", "fish", "goose", "rat", "mammoth", "zebra", "walrus", "tiger",
				"chicken", "bull", "goat", "hedgehog", "horse", "rabbit", "rhinoceros", 
				"snail", "spider", "bee", "squirrel", "owl", "guineapig", "hamster", "dog", 
				"butterfly", "dolphin", "hippo", "whale", "bird", "frog", "goldfish", "parrot"};
		String[] de0 = { "die Kuh", "die Krabbe", "das Krokodil", "der Elefant",
				"die Giraffe", "der L�we", "der Affe", "die Maus", "der Krake",
				"der Panda", "der Pinguin", "der Schwein", "das Schaf",
				"die Schlange", "die Schildkr�te", "der B�r", "die Katze",
				"der Esel", "der Hai", "die Ente", "der Fisch", "die Gans", 
				"die Ratte", "das Mammut", "das Zebra", "das Walross", "der Tiger", "das Huhn", 
				"der Stier", "die Ziege", "der Igel", "das Pferd", "der Hase","das Nashorn", 
				"die Schnecke", "die Spinne", "die Biene", "das Eichh�rnchen", "die Eule", 
				"das Meerschweinchen", "der Hamster", "der Hund", "der Schmetterling", "der Delfin", 
				"das Nilpferd", "der Wal", "der Vogel", "der Frosch", "der Goldfisch", "der Papagei"};
		
		String[] sk1 = { "�aty", "�iltovka", "kab�t", "leg�ny", "suk�a", "bl�zka", "pono�ky", "bunda", 
				"nohavi�ky", "�upan", "nohavice", "klob�k", "sveter", "tepl�kov� s�prava", "ko�e�a", 
				"pul�ver", "slipy", "d��nsy", "��l", "no�n� ko�e�a", "pan�uchy", "tielko", "sako", 
				"kr�tke tri�ko", "pr�ipl᚝", "�ortky", "sand�le", "plavky", "opasok", "tenisky", 
				"prste�", "rukavice", "top�nky", "okuliare", "gum�ky", "n�hrdeln�k", "n�u�nice", 
				"papu�e", "py�amo"};
		String[] cz1 = { "�aty", "k�iltovka", "kab�t", "leg�ny", "sukn�", "halenka", "pono�ky", 
				"bunda", "kalhotky", "�upan", "kalhoty", "klobouk", "svetr", "tepl�kov� souprava", 
				"ko�ile", "pulovr", "slipy", "d��ny", "��la", "no�n� ko�ile", "pun�ochy", "t�lko", 
				"sako", "kr�tk� tri�ko", "pl�t�nka", "�ortky", "sand�ly", "plavky", "opasek", "tenisky", 
				"prsten", "rukavice", "boty", "br�le", "hol�nky", "n�hrdeln�k", "n�u�nice", "pantofle", 
				"py�amo"};
		String[] en1 = { " dress", "cap", "coat", "leggings", "skirt", "blouse", "socks", "jacket", 
				"pants", "robe", "trousers", "hat", "sweater", "tracksuit", "shirt", "pullover", "briefs", 
				"jeans", "scarf", "night-dress", "tights", "vest", "blaser", "T-shirt", "raincoat", 
				"shorts", "sandals", "swimwear", "belt", "trainers", "ring", "gloves", "boots", "glasses", 
				"gumboots", "necklace", "earrings", "slippers", "pyjamas"};
		String[] de1 = { "das Kleid", "die Kaskett", "der Mantel", "die Leggings", "der Rock", "die Bluse", 
				"die Socken", "die Jacke", "der Slip", "der Gespan", "die Hose", "der Hut", "der Sweater", 
				"der Trainingsanzug", "das Hemd", "der Pullover", "die Unterhose", "die Jeans", "die Schal", 
				"das Nachtkleid", "die Str�mpfe", "das Trikot", "der Sakko", "das T-shirt", "die Regenjacke", 
				"die Shorts", "die Sandalen", "die Badehose", "der G�rtel", "die Turnschuhe", "der Ring", 
				"die Handschuhe", "die Schuhe", "die Brille", "der Gummistiefel", "die Halskette", 
				"die Ohrringe", "die Hausschuhe", "der Pyjama"};
	
		String[] sk2 = {"jahoda", "kiwi", "pomaran�", "anan�s", "mel�n", "limetka", "brosky�a", "jablko", 
				"malina", "slivka", "�u�oriedka", "ban�n", "hrozno", "citr�n", "�ere��a", "kokos", "zemiak", 
				"paprika", "cibu�a", "mrkva", "tekvica", "kaler�b", "cesnak", "uhorka", "karfiol", "hrach", 
				"re�kovka", "brokolica", "hr�b", "raj�ina", "kapusta", "kukurica", "med", "m�so", "torta", 
				"chlieb", "kol��", "maslo", "zmrzlina", "k�va", "cukrovinka", "cere�lie", "syr", "�okol�da", 
				"�unka", "vajce", "hranolky", "v�no", "ovocie", "zelenina"};
		String[] cz2 = {"jahoda", "kiwi", "pomeran�", "ananas", "meloun", "limetka", "broskev", "jablko", 
				"malina", "�vestka", "bor�vka", "ban�n", "hrozny", "citron", "t�e�e�", "kokos", "brambora", 
				"paprika", "cibule", "mrkev", "d�n�", "kedlubny", "�esnek", "okurka", "kv�t�k", "hr�ch", 
				"�edkvi�ka", "brokolice", "h�ib", "raj�e", "zel�", "kuku�ice", "med", "maso", "dort", "chl�b", 
				"kol��", "m�slo", "zmrzlina", "k�va", "cukrovinka", "cere�lie", "s�r", "�okol�da", "�unka", 
				"vejce", "hranolky", "v�no", "ovoce", "zelenina"};
		String[] en2 = {"strawberry", "kiwi", "orange", "pineapple", "melon", "lime", "peach", "apple", 
				"raspberry", "plum", "blueberry", "banana", "grapes", "lemon", "cherry", "coconut", "potato",
				"pepper", "onion", "carrots", "pummpkin", "kohlrabi", "garlic", "cucumber", "cauliflower", 
				"peas", "radish", "broccoli", "mushroom", "tomato", "cabbage", "corn", "honey", "meat", 
				"cake", "bread", "pie", "butter", "ice cream", "coffee", "sweet", "cereals", "cheese", 
				"chocolate", "ham", "egg", "fries", "wine", "fruit", "vegetable"};
		String[] de2 = {"die Erdbeere", "der Kiwi", "die Orange", "die Ananas", "die Melone", "die Limette",
				"der Pfirsich", "der Apfel", "die Himbeere", "die Pflaume", "die Heidelbeere", "die Banane", 
				"die Traube", "die Zitrone", "die Kirsche", "die Kokosnuss", "die Kartoffel", "der Paprika", 
				"die Zwiebel", "die Karotte", "der K�rbis", "der Kohlrabi", "der Knoblauch", "die Gurke", 
				"der Blumenkohl", "die Erbse", "das Radieschen", "das Brokkoli", "der Pilz", "die Tomate", 
				"der Kohl", "der Mais", "der Honig", "das Fleisch", "die Torte", "das Brot", "der Kuchen", 
				"die Butter", "das Eis", "der Kaffee", "die S��ware", "die Zerealien", "der K�se", 
				"die Schokolade", "der Schinken", "das Ei", "die Pommes frites", "der Wein", "das Obst", "das Gem�se"};
		
		String [] sk = null, cz = null, en = null, de = null;
		super.paintComponent(g);

		Font font = new java.awt.Font("SansSerif", Font.PLAIN, 20);
		Color fialova;
		
		if(defLang == lang2)
			fialova = new Color(0, 100, 255);
		else
			fialova = new Color(255, 0, 100);
		
		FontMetrics metrics = g.getFontMetrics(font);
		
		switch (theme)
		{
			case 0:
				sk=sk0;
				cz=cz0;
				en=en0;
				de=de0;
				break;
			case 1:
				sk=sk1;
				cz=cz1;
				en=en1;
				de=de1;
				break;
			case 2:
				sk=sk2;
				cz=cz2;
				en=en2;
				de=de2;
				break;
		}
		if(!clicked)
		{
			g.setColor(Color.BLACK);
			g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
			g.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
			g.setFont(font);
			g.setColor(fialova);
			switch (lang)
			{
				case 0:
					if (lang2 == 0)
						g.drawString(sk[part], getWidth() / 2 - metrics.stringWidth(sk[part]) / 2, getHeight() / 2);
					else
						g.drawString(en[part], getWidth() / 2 - metrics.stringWidth(en[part]) / 2, getHeight() / 2);
					break;
				case 1:
					if (lang2 == 0)
						g.drawString(sk[part], getWidth() / 2 - metrics.stringWidth(sk[part]) / 2, getHeight() / 2);
					else
						g.drawString(de[part], getWidth() / 2 - metrics.stringWidth(de[part]) / 2, getHeight() / 2);
					break;
				case 2:
					if (lang2 == 0)
						g.drawString(sk[part], getWidth() / 2 - metrics.stringWidth(sk[part]) / 2, getHeight() / 2);
					else
						g.drawString(cz[part], getWidth() / 2 - metrics.stringWidth(cz[part]) / 2, getHeight() / 2);
					break;
				case 3:
					if (lang2 == 0)
						g.drawString(de[part], getWidth() / 2 - metrics.stringWidth(de[part]) / 2, getHeight() / 2);
					else
						g.drawString(en[part], getWidth() / 2 - metrics.stringWidth(en[part]) / 2, getHeight() / 2);
					break;
				case 4:
					if (lang2 == 0)
						g.drawString(cz[part], getWidth() / 2 - metrics.stringWidth(cz[part]) / 2, getHeight() / 2);
					else
						g.drawString(en[part], getWidth() / 2 - metrics.stringWidth(en[part]) / 2, getHeight() / 2);
					break;
				case 5:
					if (lang2 == 0)
						g.drawString(de[part], getWidth() / 2 - metrics.stringWidth(de[part]) / 2, getHeight() / 2);
					else
						g.drawString(cz[part], getWidth() / 2 - metrics.stringWidth(cz[part]) / 2, getHeight() / 2);
					break;
			}

		}
		else
		{
			
			g.setColor(Color.BLACK);
			g.drawImage(pexeso, 0, 0, getWidth(), getHeight(), null);
			g.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
		}
	}
	
	public void hidePexeso(){
		clicked = false;
		repaint();
	}
	
	public void showPexeso(){
		clicked = true;
		repaint();
	}

	@Override
	public void mouseClicked(MouseEvent e)
	{
		clicked = true;
		repaint();
		
		if (listener !=null)
			listener.OnClicked(this);
	}

	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e)
	{
		// TODO Auto-generated method stub
		
	}
	
	public void setOnClickListener(OnClick onClickListener)
	{
		listener=onClickListener;
	}
	public abstract interface OnClick
	{
		abstract void OnClicked(Picture pict);
	}
	
}