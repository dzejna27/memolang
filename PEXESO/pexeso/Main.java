package pexeso;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Main
{
	public static void CreateMenu(final JFrame frame)
	{
		String[] langs = {
				"<html><p>Sloven�ina - Angli�tina<br />English-Slovak</p></html>",
				"<html><p>Sloven�ina - Nem�ina<br />Deutch-Slowakisch</p></html>",
				"<html><p>Sloven�ina - �e�tina<br />�e�tina-Sloven�ina</p></html>",
				"<html><p>English - German<br />Deutch-Englisch</p></html>",
				"<html><p>�e�tina-Angli�tina<br />English - Czech</p></html>",
				"<html><p>�estina - Nem�ina<br />Deutch - Tschechisch</p></html>" };
		final String[] players = { "Pocet hr��ov / Number of players",
				"Po�et hr��ov / die Anzahl den Spielern", "Po�et hr��ov / Po�et hr���",
				"Number of players / die Anzahl den Spielern","Po�et hr��� / Number of players",
				"Po�et hr��� / die Anzahl den Spielern" };

		JImagePanel background = new JImagePanel();
		Box box = Box.createVerticalBox();
		box.add(Box.createVerticalStrut(150));
		JButton[] button = new JButton[langs.length];

		for (Integer i = 0; i < langs.length; i++)
		{
			final Integer lang = new Integer(i);
			button[i] = new JButton(langs[i]);

			ActionListener action = new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					JFrame frame2 = new JFrame(players[lang]);

					CreateMenu2(frame2, lang);

					frame2.setSize(800, 600);
					frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					frame2.setVisible(true);
					frame.setVisible(false);
				}
			};

			button[i].addActionListener(action);
			box.add(button[i]);
			box.add(Box.createVerticalStrut(10));
		}
		background.add(box);
		frame.add(background);
	}

	public static void CreateMenu2(final JFrame frame, final int lang)
	{
		final String[] level = { "N�ro�nos� / Difficulty","N�ro�nos� / Die Anspr�che",
				"N�ro�nos� / N�ro�nost","Difficulty / Die Anspr�che",
				"N�ro�nost / Difficulty","N�ro�nost / Die Anspr�che"};
		
		String[] sk_cz_en = { "<html><p>1 hr�� / 1 player</p></html>",
				"<html><p>2 hr��i / 2 players</p></html>",
				"<html><p>3 hr��i / 3 players</p></html>" };
		String[] sk_cz_de = { "<html><p>1 hr�� / 1 Spieler",
				"<html><p>2 hr��i / 2 Spieler", "<html><p>3 hr��i / 3 Spieler" };
		String[] sk_cz = { "<html><p>1 hr��</p></html>",
				"<html><p>2 hr��i</p></html>", "<html><p>3 hr��i</p></html>" };
		String[] en_de = { "<html><p>1 player/ 1 Spieler</p></html>",
				"<html><p>2 players/ 2 Spieler</p></html>",
				"<html><p>3 players/ 3 Spieler</p></html>" };

		JImagePanel background = new JImagePanel();
		Box box = Box.createVerticalBox();
		box.add(Box.createVerticalStrut(150));
		JButton[] button = new JButton[3];

		for (Integer i = 0; i < 3; i++)
		{
			final Integer j = new Integer(i);
			switch (lang)
			{
			case 0:
			case 4:
				button[i] = new JButton(sk_cz_en[i]);
				break;
			case 1:
			case 5:
				button[i] = new JButton(sk_cz_de[i]);
				break;
			case 2:
				button[i] = new JButton(sk_cz[i]);
				break;
			case 3:
				button[i] = new JButton(en_de[i]);
				break;
			}
			ActionListener action = new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					JFrame frame2 = new JFrame(level[lang]);

					CreateMenu3(frame2, lang, j);

					frame2.setSize(800, 600);
					frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					frame2.setVisible(true);
					frame.setVisible(false);
				}
			};
			button[i].addActionListener(action);
			box.add(button[i]);
			box.add(Box.createVerticalStrut(50));
		}
		background.add(box);
		frame.add(background);
	}

	public static void CreateMenu3(final JFrame frame, final int lang,
			final int count)
	{
		final String [] theme = { "T�ma / Theme", "T�ma / Das Thema", "T�ma",
				"Theme / Das Thema", "T�ma / Theme", "T�ma/ Das Theme"};
	
		String[] sk_en = { "<html><p>�ahk� / easy</p></html>",
				"<html><p>stredn� / medium</p></html>",
				"<html><p>�a�k� / hard</p></html>" };
		String[] sk_de = { "<html><p>�ahk� / leicht</p></html>",
				"<html><p>stredn� / mittel</p></html>",
				"<html><p>�a�k� / hart</p></html>" };
		String[] sk_cz = { "<html><p>�ahk� / snadn�</p></html>",
				"<html><p>stredn� / st�edn�</p></html>",
				"<html><p>�a�k� / t�k�</p></html>" };
		String[] en_de = { "<html><p>easy / leicht</p></html>",
				"<html><p>medium / mittel</p></html>",
				"<html><p>hard / hart</p></html>" };
		String[] cz_en = { "<html><p>snadn� / easy</p></html>",
				"<html><p>st�edn� / medium</p></html>",
				"<html><p>t�k� / hard</p></html>" };
		String[] cz_de = { "<html><p>snadn� / leicht</p></html>",
				"<html><p>st�edn� / mittel</p></html>",
				"<html><p>t�k� / hart</p></html>" };

		JImagePanel background = new JImagePanel();
		Box box = Box.createVerticalBox();
		box.add(Box.createVerticalStrut(150));
		JButton[] button = new JButton[3];

		for (Integer i = 0; i < 3; i++)
		{
			final Integer diff = new Integer(i);
			switch (lang)
			{
			case 0:
				button[i] = new JButton(sk_en[i]);
				break;
			case 1:
				button[i] = new JButton(sk_de[i]);
				break;
			case 2:
				button[i] = new JButton(sk_cz[i]);
				break;
			case 3:
				button[i] = new JButton(en_de[i]);
				break;
			case 4:
				button[i] = new JButton(cz_en[i]);
				break;
			case 5:
				button[i] = new JButton(cz_de[i]);
				break;
			}
			ActionListener action = new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					JFrame frame2 = new JFrame(theme[lang]);

					CreateMenu4(frame2, lang, count, diff);

					frame2.setSize(800, 600);
					frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					frame2.setVisible(true);
					frame.setVisible(false);
				}
			};
			button[i].addActionListener(action);
			box.add(button[i]);
			box.add(Box.createVerticalStrut(50));
		}
		background.add(box);
		frame.add(background);
	}

	public static void CreateMenu4(final JFrame frame, final int lang, final int count, final int diff)
	{
		String[] sk_en = { "<html><p>zvierat� / animals</p></html>",
				"<html><p>oble�enie / clothes</p></html>",
				"<html><p>jedlo / food</p></html>"};
		String[] sk_de = { "<html><p>zvierat� / die Tiere</p></html>",
				"<html><p>oble�enie / die Kleidung</p></html>",
				"<html><p>jedlo / das Essen</p></html>"};
		String[] sk_cz = { "<html><p>zvierat� / zv��ata</p></html>",
				"<html><p>oble�enie / oble�en�</p></html>",
				"<html><p>jedlo / j�dlo</p></html>"};
		String[] en_de = { "<html><p>animals / die Tiere</p></html>",
				"<html><p>clothes / die Kleidung</p></html>",
				"<html><p>food / das Essen</p></html>"};
		String[] cz_en = { "<html><p>zv��ata /  animals</p></html>",
				"<html><p>oble�en� / clothes</p></html>",
				"<html><p>j�dlo / food</p></html>"};
		String[] cz_de = { "<html><p>zv��ata /  die Tiere</p></html>",
				"<html><p>oble�en� / die Kleidung</p></html>",
				"<html><p>j�dlo / das Essen</p></html>"};
		
		JImagePanel background = new JImagePanel();
		Box box = Box.createVerticalBox();
		box.add(Box.createVerticalStrut(150));
		JButton[] button = new JButton[3];

		for (Integer i = 0; i < 3; i++)
		{
			final Integer theme = new Integer(i);
			switch (lang)
			{
			case 0:
				button[i] = new JButton(sk_en[i]);
				break;
			case 1:
				button[i] = new JButton(sk_de[i]);
				break;
			case 2:
				button[i] = new JButton(sk_cz[i]);
				break;
			case 3:
				button[i] = new JButton(en_de[i]);
				break;
			case 4:
				button[i] = new JButton(cz_en[i]);
				break;
			case 5:
				button[i] = new JButton(cz_de[i]);
				break;
			}
			ActionListener action = new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					JFrame frame2 = new JFrame("Info");

					CreateMenu5(frame2, lang, count, diff, theme);

					frame2.setSize(800, 600);
					frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					frame2.setVisible(true);
					frame.setVisible(false);
				}
			};
			button[i].addActionListener(action);
			box.add(button[i]);
			box.add(Box.createVerticalStrut(50));
		}
		background.add(box);
		frame.add(background);
	}

	public static void CreateMenu5(final JFrame frame, final int lang, final int count, final int diff, final int theme)
	{
		String sk_en0 = "Ktor� jazyk sa chce� u�i�? / Which language do you want to learn?";
		String sk_de0 = "Ktor� jazyk sa chce� u�i�? / Welche Sprache mochtest du lernen?";
		String sk_cz0 = "Ktor� jazyk sa chce� u�i�? / Kter� jazyk se chce� u�it?";
		String en_de0 = "Which language do you want to learn? / Welche Sprache mochtest du lernen?";
		String cz_en0 = "Kter� jazyk se chce� u�it? / Which language do you want to learn?";
		String cz_de0 = "Kter� jazyk se chce� u�it? / Welche Sprache mochtest du lernen?";

		String[] sk_en = { "<html><p>angli�tina / English</p></html>",
				"<html><p>sloven�ina / slovak</p></html>" };
		String[] sk_de = { "<html><p>nem�ina / Deutch</p></html>",
				"<html><p>sloven�ina / Slovak</p></html>" };
		String[] sk_cz = { "<html><p>�e�tina</p></html>",
				"<html><p>sloven�ina</p></html>" };
		String[] en_de = { "<html><p>German/ Deutch</p></html>",
				"<html><p>English / Englisch</p></html>" };
		String[] cz_en = { "<html><p>angli�tina / English</p></html>",
				"<html><p>�e�tina / Czech</p></html>" };
		String[] cz_de = { "<html><p>nem�ina / Deutch</p></html>",
				"<html><p>�e�tina / Tschechisch</p></html>" };

		String sk_en2 = "<html><p>Aby si sa slov��ka nau�il dobre, ako prv� bude� m�c� ot��a� karti�ky v rodnom jazyku ozna�en� modrou farbou.<br /><br />"
				+ "To learn the words effectively, firstly you will be allowed to turn over the blue cards, which will be written in you mother tongue.</p></html>";
		String sk_de2 = "<html><p>Aby si sa slov��ka nau�il dobre, ako prv� bude� m�c� ot��a� karti�ky v rodnom jazyku ozna�en� modrou farbou.<br /><br />"
				+ "Als dass du die Worter gut einzulernt, kannst du als die erste die Karten in deiner heimatlichen Sprache in blauer Farbe umdrehen.</p></html>";
		String sk_cz2 = "<html><p>Aby ses slov��ka nau�il dob�e, jako prvn� bude� moci ot��et karti�ky v rodn�m jazyce ozna�en� modrou barvou.<br /><br />"
				+ "To learn words very good, you will be able to rotate cards in maternity language as first. They wil have blue color</p></html>";
		String en_de2 = "<html><p>To learn the words effectively, firstly you will be allowed to turn over the blue cards, which will be written in you mother tongue.<br /><br />"
				+ "Als dass du die Worter gut einzulernt, kannst du als die erste die Karten in deiner heimatlichen Sprache in blauer Farbe umdrehen.</p></html>";
		String cz_en2 = "<html><p>Aby ses slov��ka nau�il dob�e, jako prvn� bude� moci ot��et karti�ky v rodn�m jazyce ozna�en� modrou barvou.<br /><br />"
				+ "To learn the words effectively, firstly you will be allowed to turn over the blue cards, which will be written in you mother tongue.</p></html>";
		String cz_de2 = "<html><p>Aby ses slov��ka nau�il dob�e, jako prvn� bude� moci ot��et karti�ky v rodn�m jazyce ozna�en� modrou barvou.<br /><br />"
				+ "Als dass du die Worter gut einzulernt, kannst du als die erste die Karten in deiner heimatlichen Sprache in blauer Farbe umdrehen.</p></html>";

		JImagePanel background = new JImagePanel();
		Box box = Box.createVerticalBox();
		box.add(Box.createVerticalStrut(150));
		JButton[] button = new JButton[3];
		JLabel lab1 = null, lab2 = null;

		switch (lang)
		{
		case 0:
			lab1 = new JLabel(sk_en0);
			lab2 = new JLabel(sk_en2);
			break;
		case 1:
			lab1 = new JLabel(sk_de0);
			lab2 = new JLabel(sk_de2);
			break;
		case 2:
			lab1 = new JLabel(sk_cz0);
			lab2 = new JLabel(sk_cz2);
			break;
		case 3:
			lab1 = new JLabel(en_de0);
			lab2 = new JLabel(en_de2);
			break;
		case 4:
			lab1 = new JLabel(cz_en0);
			lab2 = new JLabel(cz_en2);
			break;
		case 5:
			lab1 = new JLabel(cz_de0);
			lab2 = new JLabel(cz_de2);
			break;
		}

		lab1.setMaximumSize(new Dimension(400, 300));
		lab2.setMaximumSize(new Dimension(400, 300));
		box.add(lab1);
		box.add(Box.createVerticalStrut(15));
		box.add(lab2);
		box.add(Box.createVerticalStrut(30));

		for (Integer i = 0; i < 2; i++)

		{
			final Integer defLang = new Integer(i);
			switch (lang)
			{
			case 0:
				button[i] = new JButton(sk_en[i]);
				break;
			case 1:
				button[i] = new JButton(sk_de[i]);
				break;
			case 2:
				button[i] = new JButton(sk_cz[i]);
				break;
			case 3:
				button[i] = new JButton(en_de[i]);
				break;
			case 4:
				button[i] = new JButton(cz_en[i]);
				break;
			case 5:
				button[i] = new JButton(cz_de[i]);
				break;
			}
			ActionListener action = new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					Box box = Box.createVerticalBox();
					JGameStatus status = new JGameStatus(700, lang);
					JFrame frameg = new JFrame("Game");
					frameg.setSize(700, 750);
					box.add(status);
					box.add(new Game(lang, count, diff, theme, status, defLang));
					frameg.add(box);
					frameg.setVisible(true);
					frame.setVisible(false);
				}
			};
			button[i].addActionListener(action);
			box.add(button[i]);
			box.add(Box.createVerticalStrut(30));
		}
		background.add(box);
		frame.add(background);
	}

	public static void main(String[] args)
	{
		JFrame frame = new JFrame();
		frame.setSize(800, 600);
		frame.setTitle("Languages");
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		CreateMenu(frame);
		frame.setVisible(true);

	}
}