package pexeso;

import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

public class JGameStatus extends JPanel
{
	public int player = 1, finded = 0, lang; 
	
	public JGameStatus(int width, int lang)
	{
		setMaximumSize(new Dimension(width, 30));
		setPreferredSize(new Dimension(width, 30));
		this.lang=lang;
	}
	
	protected void paintComponent(Graphics g)
	{
		String [] plr={"hr�� / player", "hr�� / Spieler" , "hr��", "player / Spieler", "hr�� / player", "hr�� / Spieler"};
		String [] point={"bod / point", "bod / Punkt", "bod", "point / Punkt", "bod / point, bod / Punkt"};
		String [] points24={"body / points", "body / Punkte", "body", "points / Punkte", "body / points, body / Punkte"};	
		String [] points5={"bodov / points", "bodov / Punkte", "bodov / bod�", "points / Punkte", "bod� / points, bod� / Punkte"};	
		String pts = "";
		
		super.paintComponent(g);
		
		if (finded==1)
			pts = point[lang];
		else
		{	
			if ((finded>4) || (finded==0))
				pts = points5[lang];
			else
				pts = points24[lang];
		}
		g.drawString(plr[lang] + " " + player, 10, getHeight() / 2);
		g.drawString(finded + " " + pts, getWidth() / 2, getHeight() / 2);
	}
}
